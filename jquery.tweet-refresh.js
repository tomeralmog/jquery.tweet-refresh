/*
	https://bitbucket.org/tomeralmog/jquery.tweet-refresh
	Autor:Tomer Almog https://bitbucket.org/tomeralmog/
	License: MIT
*/
(function ( $ ) {
	$.fn.tweetRefresh = function(options) {
		 var settings = $.extend({
		// These are the defaults.
			inputID: '#tweet-text',//textarea element
			tweetButtonID: '#tweetButton',//the submit button
			refreshID: '#refresh-tweet',//get a new message
			tweetMsgArr:[],//message to tweet
			tweetbitlyArr:[],//urls to each msg
			newWinowWidth:575,//twitter window
			newWindowHeight:400,//twitter window
			hashtag:''
			
		
		}, options );
		
		var tweetSettings={
			msgIndex:0,
			bitlyIndex:0,
			inputElem:  $(settings.inputID),
			tweetElem:  $(settings.tweetButtonID),
			refreshElem:  $(settings.refreshID),
			winWidth:$(window).width(),
			winHeight:$(window).height()
			
		};
		$(window).resize(function(){
			tweetSettings.winWidth = $(window).width();
			tweetSettings.winHeight = $(window).height();
		})
		
		
		 return this.each(function() {
			
									
		tweetSettings.refreshElem.on('click',function(){
			if(tweetSettings.msgIndex==settings.tweetMsgArr.length){
				tweetSettings.msgIndex=0;
			}
			tweetSettings.inputElem.val(settings.tweetMsgArr[tweetSettings.msgIndex]);
			tweetSettings.bitlyIndex = tweetSettings.msgIndex;
			tweetSettings.msgIndex++;
		});
		
		
		
		
		tweetSettings.tweetElem.click(function(event) {	
		var width  = settings.newWinowWidth,
			height = settings.newWindowHeight,
			left   = (tweetSettings.winWidth  - width)  / 2,
			top    = (tweetSettings.winHeight - height) / 2,
			url    = 'https://twitter.com/share?text=',
			opts   = 'status=1' +
					 ',width='  + width  +
					 ',height=' + height +
					 ',top='    + top    +
					 ',left='   + left;
					 
					 
		
		var msgToTweet = encodeURIComponent(tweetSettings.inputElem.val());	
					 
		
		window.open(url+msgToTweet+'&hashtags='+settings.hashtag+'&url='+settings.tweetbitlyArr[tweetSettings.bitlyIndex], 'twitter', opts);
		event.preventDefault();
		return false;
	  });
		
		
						
		
		tweetSettings.refreshElem.trigger('click');	
			
		
		});
	};
}( jQuery ));




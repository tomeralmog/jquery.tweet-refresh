# [Jquery.Tweet-Refresh](https://bitbucket.org/tomeralmog/jquery.tweet-refresh/)

A Simple plugin to refresh tweets and share them form a textarea element.

##Requires

* JQuery

##Installation
Include script after the jQuery library (unless you are packaging scripts somehow else):

    <script src="/path/to/jquery.tweety-refresh.min.js"></script>


##Usage

    $(selector).tweetRefresh(options);
Options:


- inputID:       
	- default:'#tweet-text'    
	- description: textarea element ID
- tweetButtonID: 
	- default:'#tweetButton',
	- description: the submit button - tweet the current text
- refreshID: 
	- defualt: '#refresh-tweet'
	- description: button to generate a new message
- tweetMsgArr:
	- default:[]
	- description: array of message to tweet
- tweetbitlyArr:
	- default: []
	- description: array of urls to each message
- newWinowWidth:
	- default: 575
	- description: twitter new window width
- newWindowHeight:
	- default: 400
	- description: twitter new window height
- hashtag:
	- default:''
	- description: hashtag for the message

###Example of usage:


    $(selector).tweetRefresh({
 
		inputID:"#textareaID",
		tweetButtonID: "#tweetButtonID",
		refreshID: "#refreshID",
		tweetMsgArr:["msg1","msg2"],
		tweetbutlyArr:["url1","url2"],
		newWindowWidth:575,
		newWindowHeight:400,
		hashtag:"myHashTag"
	});


## Author


[Tomer Almog](https://bitbucket.org/tomeralmog)

## License
[MIT](http://en.wikipedia.org/wiki/MIT_License)